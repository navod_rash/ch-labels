<?php

return [
    'Create Label' => 'Create Label',
    'id' => 'id',
    'Label' => 'Label',
    'Color' => 'Color',
    'Plugin' => 'Plugin',
    'Update' => 'Update',
    'Delete' => 'Delete',

    'Update Label' => 'Update Label',
    'Create new label' => 'Create new label',
    'Name' => 'Name',
    'Color' => 'Color',
    'Target' => 'Target',
    'select a plugin' => 'select a plugin',

    '404' => 'Label is not found.'
];
