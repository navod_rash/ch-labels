<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLabelsRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasTable('labels_relation')) {
            Schema::create('labels_relation', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('model_id')->nullable();
                $table->string('model')->nullable();
                $table->integer('label_id')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('label_id')->references('id')->on('labels');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('labels_relation');
    }
}
