<?php

namespace Creativehandles\ChLabels\Plugins\Labels;

use Creativehandles\ChLabels\Plugins\Labels\Repositories\LabelRepository;
use Creativehandles\ChLabels\Plugins\Plugin;

class Labels extends Plugin
{
    /**
     * @var LabelRepository
     */
    private $labelRepository;

    public function __construct(LabelRepository $labelRepository)
    {
        parent::__construct();

        $this->labelRepository = $labelRepository;
    }

    public function createOrUpdateLabel($data, $condition)
    {
        return $this->labelRepository->updateOrCreateByMultipleKeys($condition, $data);
    }

    public function getAllLabels()
    {
        return $this->labelRepository->all();
    }
}
