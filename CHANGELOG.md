# Changelog

All notable changes to `ch-labels` will be documented in this file

## 1.0.0 - 2020-03-21

- initial release of ch-labels plugin

## 1.0.1 - 2020-03-22

- updated readme.md

## 1.1.0 - 2020-03-22

- added command to build all prerequisites 
- updated readme.md
- added options to automatically catch routes and breadcrumbs when installing plugin into core-cms project
- added travis ci support

# 1.2.0 - 2020-03-22
- added style ci code coverage
- auto publishing vendor files

#2.0.0 - 2020-04-03
- added controller that extends cor controller
- moved core classes into src
- publish only config, routes and the views